
#server-setup.sh 
#Bash script for installing a web server and related dependencies, as well as configuring vim
#Copyright (C) 2016,2017 Daniel Fredriksen
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.



apt-get install nginx
apt-get install fail2ban
apt-get install php7.0-fpm
apt-get install php7.0-curl
apt-get install letsencrypt
apt-get install mysql-server
mysql_secure_installation
apt-get install php7.0-mcrypt
apt-get install php7.0-zip
apt-get install phpmyadmin
apt-get install composer
apt-get install nodejs-legacy
apt-get install npm
apt-get install zip
echo 'set tabstop=4 shiftwidth=4 expandtab' | tee -a /root/.vimrc
echo "set viminfo='100,<1000,s100,h" | tee -a /root/.vimrc
